package com.sgcib.bankaccount;

import com.sgcib.bankaccount.domain.Account;
import com.sgcib.bankaccount.domain.common.exceptions.OperationNotAllowedException;
import com.sgcib.bankaccount.domain.port.IPrintHistory;
import com.sgcib.bankaccount.infrastructure.StatementHistoryPrinterImpl;

import java.time.Clock;

public class BankAccountApplication {

    public static void main(String[] args) throws OperationNotAllowedException {

        //Instantiate hexagon
        Account accountPort = new Account(Clock.systemDefaultZone());

        accountPort.deposit(5000);
        accountPort.deposit(1500);
        accountPort.withdraw(1200);
        accountPort.withdraw(500);

        IPrintHistory statementHistoryPrinter = new StatementHistoryPrinterImpl();
        accountPort.printHistory(statementHistoryPrinter);

    }
}
