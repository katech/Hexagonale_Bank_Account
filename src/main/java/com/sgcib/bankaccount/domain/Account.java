package com.sgcib.bankaccount.domain;

import com.sgcib.bankaccount.domain.common.EBankOperations;
import com.sgcib.bankaccount.domain.common.exceptions.OperationNotAllowedException;
import com.sgcib.bankaccount.domain.port.IPrintHistory;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

public class Account {

    private final Clock clock;

    private final StatementsHistory history = new StatementsHistory();

    private Balance balance = new Balance(0);

    public Account(Clock clock) {
        this(clock, 0.0);
    }

    public Account(Clock clock, double balance) {
        this.clock = clock;
        this.balance = new Balance(balance);
    }

    /**
     *
     * @return Balance
     */
    public Balance getBalance() {
        return this.balance;
    }


    /**
     * @param amount
     */
    public void deposit(double amount) throws OperationNotAllowedException {
        updateAccount(amount, EBankOperations.DEPOSIT);
    }

    public void withdraw(double amount) throws OperationNotAllowedException {
        updateAccount(amount, EBankOperations.WITHDRAWAL);
    }

    private void updateAccount(double amount, EBankOperations operationType) throws OperationNotAllowedException {
        if(amount < 0){
            throw new IllegalArgumentException("The amount must be positive");
        }
        Amount amount1 = new Amount(amount);
        balance = balance.updateBalance(operationType, amount1);
        addToHistory(operationType, LocalDateTime.now(clock), amount1, balance);
    }

    /**
     *
     * @param statementHistoryPrinter
     */
    public void printHistory(IPrintHistory statementHistoryPrinter) {
        history.print(statementHistoryPrinter);
    }

    /**
     *
     * @param operationType
     * @param operationDate
     * @param amount
     * @param balance
     */
    private void addToHistory(EBankOperations operationType, LocalDateTime operationDate, Amount amount, Balance balance) {
        history.addEntry(operationType, operationDate, amount, balance);
    }

    public List<StatementHistoryEntry> getHistory() {
        return history.getEntries();
    }

}
