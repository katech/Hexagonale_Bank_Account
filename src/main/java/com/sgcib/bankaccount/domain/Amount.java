package com.sgcib.bankaccount.domain;

public record Amount(double valueAmount) {}
