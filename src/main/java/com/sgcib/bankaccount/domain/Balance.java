package com.sgcib.bankaccount.domain;

import com.sgcib.bankaccount.domain.common.EBankOperations;
import com.sgcib.bankaccount.domain.common.exceptions.OperationNotAllowedException;

public record Balance(double valueBalance) {

    public Balance updateBalance(EBankOperations operationType, Amount amount) throws OperationNotAllowedException {
        switch (operationType) {
            case DEPOSIT :
                return new Balance(valueBalance + amount.valueAmount());
            case WITHDRAWAL :
                if(valueBalance < amount.valueAmount()){
                    throw OperationNotAllowedException.create(amount.valueAmount(), valueBalance);
                }
                return new Balance(valueBalance - amount.valueAmount());
            default:
                throw new IllegalArgumentException("Operation type not allowed");
        }
    }

}
