package com.sgcib.bankaccount.domain;

import com.sgcib.bankaccount.domain.common.EBankOperations;

import java.time.LocalDateTime;

public record StatementHistoryEntry(EBankOperations operationType, LocalDateTime operationDate, Amount amount, Balance balance) {}
