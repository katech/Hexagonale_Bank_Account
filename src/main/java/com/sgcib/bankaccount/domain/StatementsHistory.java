package com.sgcib.bankaccount.domain;

import com.sgcib.bankaccount.domain.common.EBankOperations;
import com.sgcib.bankaccount.domain.port.IPrintHistory;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class StatementsHistory {

    private List<StatementHistoryEntry> entries = new LinkedList<>();

    /**
     * @param operationType
     * @param operationDate
     * @param amount
     * @param balance
     */
    public void addEntry(EBankOperations operationType, LocalDateTime operationDate, Amount amount, Balance balance) {
        this.entries.add(new StatementHistoryEntry(operationType, operationDate, amount, balance));
    }

    /**
     *
     * @return List of StatementHistoryEntry
     */
    public List<StatementHistoryEntry> getEntries() {
        return entries;
    }

    /**
     * @param statementHistoryPrinter
     */
    public void print(IPrintHistory statementHistoryPrinter) {
        entries.stream()
                .forEach(e -> statementHistoryPrinter.print(e));
    }

}
