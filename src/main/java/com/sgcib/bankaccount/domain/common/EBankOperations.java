package com.sgcib.bankaccount.domain.common;

public enum EBankOperations {
    WITHDRAWAL, DEPOSIT
}
