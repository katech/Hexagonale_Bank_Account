package com.sgcib.bankaccount.domain.common.exceptions;

public class OperationNotAllowedException extends Exception {

    public OperationNotAllowedException(final String message){
        super(message);
    }
    public static OperationNotAllowedException create(final double amount, double balance){
        return new OperationNotAllowedException(String.format("Your balance is insufficient : you want to withdraw %f then your account has a balance %f", amount, balance));

    }
}
