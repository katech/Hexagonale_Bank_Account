package com.sgcib.bankaccount.domain.port;

import com.sgcib.bankaccount.domain.StatementHistoryEntry;

public interface IPrintHistory {
    void print(StatementHistoryEntry statementHistoryEntry);
}
