package com.sgcib.bankaccount.infrastructure;

import com.sgcib.bankaccount.domain.StatementHistoryEntry;
import com.sgcib.bankaccount.domain.port.IPrintHistory;

import java.util.StringJoiner;

public class StatementHistoryPrinterImpl implements IPrintHistory {

    @Override
    public void print(StatementHistoryEntry statementHistoryEntry) {
        System.out.println(formatStatementHistoryEntry(statementHistoryEntry));
    }

    /**
     * @param statementHistoryEntry
     * @return statement history
     */
    private static String formatStatementHistoryEntry(StatementHistoryEntry statementHistoryEntry) {
        return new StringJoiner(" | ")
                .add("Operation='" + statementHistoryEntry.operationType() + "'")
                .add("Date='" + statementHistoryEntry.operationDate() + "'")
                .add("Amount='" + statementHistoryEntry.amount().valueAmount() + "'")
                .add("Balance='" + statementHistoryEntry.balance().valueBalance() + "'").toString();
    }
}
