package com.sgcib.bankaccount.bdd;

import com.sgcib.bankaccount.domain.Account;
import com.sgcib.bankaccount.domain.Amount;
import com.sgcib.bankaccount.domain.common.EBankOperations;
import com.sgcib.bankaccount.domain.common.exceptions.OperationNotAllowedException;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountSteps {


    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private Account account = new Account(Clock.systemDefaultZone());
    private String lastError;

    @Given("^i have a bank account with balance (\\d+)$")
    public void i_have_a_bank_account_with_balance(Integer balance) throws Throwable {
        this.account = new Account(Clock.systemDefaultZone(), balance);
    }

    @When("^i am depositing (-?\\d+) in my account$")
    public void i_deposit(Integer amount) throws OperationNotAllowedException {
        try{
            this.account.deposit(amount);
        }catch(Exception e){
            this.lastError = e.getMessage();
        }

    }

    @Then("^my balance must be (\\d+)$")
    public void my_balance_must_be(Integer amount) {
        assertThat(this.account.getBalance().valueBalance()).isEqualTo(new Amount(amount).valueAmount());
    }

    @When("^i am withdrawing (\\d+) from my account$")
    public void i_am_withdrawing_from_my_account(double withdrawal) throws OperationNotAllowedException {
        try{
            this.account.withdraw(withdrawal);
        }catch(Exception e){
            this.lastError = e.getMessage();
        }
    }

    @DataTableType
    public HistoryLine getHistoryLine(Map<String, String> entry) {
        return HistoryLine.create(entry);
    }

    @When("i print my statement i got this output")
    public void i_print_My_Statement_I_Got_This_Output(List<HistoryLine> checkedHistoryLines) {
        var historyLines = this.account.getHistory();

        var converted = historyLines.stream()
                .map(l -> new HistoryLine(l.operationType(), DATE_TIME_FORMATTER.format(l.operationDate()), l.amount().valueAmount(), l.balance().valueBalance()))
                .toList();

        Assertions.assertThat(converted).as("The history should be the same").containsExactlyElementsOf(checkedHistoryLines);
    }

    @Given("we are the {string}")
    public void weAreThe(String date) {
        LocalDate localDateTime = LocalDate.parse(date, DATE_TIME_FORMATTER);
        Instant.now(Clock.fixed(localDateTime.atStartOfDay().toInstant(ZoneOffset.UTC), ZoneOffset.UTC));
    }

    @Then("i get the following error : {string}")
    public void iGetTheFollowingError(String message) {
        assertThat(this.lastError).isEqualTo(message);
    }

    public record HistoryLine(EBankOperations operation, String date, double amount, double balance) {
        public static HistoryLine create(Map<String, String> entry) {
            return new HistoryLine(EBankOperations.valueOf(entry.get("operation")),
                    entry.get("date"),
                    Double.parseDouble(entry.get("amount")),
                    Double.parseDouble(entry.get("balance")));
        }
    }

}
