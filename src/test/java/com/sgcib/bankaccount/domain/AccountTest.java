package com.sgcib.bankaccount.domain;

import com.sgcib.bankaccount.domain.common.EBankOperations;
import com.sgcib.bankaccount.domain.common.exceptions.OperationNotAllowedException;
import com.sgcib.bankaccount.domain.port.IPrintHistory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class AccountTest {

    private Account account;
    private String exception;

    @Mock
    private IPrintHistory statementHistoryPrinter;

    @BeforeEach
    public void init() {
        account = new Account(Clock.systemDefaultZone());
    }

    @Captor
    private ArgumentCaptor<StatementHistoryEntry> entryArgumentCapture;

    @Test
    public void should_deposit_200euros_with_success() throws OperationNotAllowedException {
        account.deposit(200);
        final Balance expectedBalance = new Balance(200);
        assertEquals(expectedBalance, account.getBalance());
    }

    @Test
    public void should_deposit_2000euros_and_5500euros_with_success() throws OperationNotAllowedException {
        account.deposit(2000);
        account.deposit(5500);
        final Balance expectedBalance = new Balance(7500);
        assertEquals(expectedBalance, account.getBalance());
    }

    @Test
    public void should_not_deposit_negative_amount() {
        Exception thrown = assertThrows(
                IllegalArgumentException.class,
                () -> account.deposit(-200));
        assertTrue(thrown.getMessage().equals("The amount must be positive"));
    }

    @Test
    public void should_getBalance_for_an_account_without_operations() {
        final Balance actualBalance = account.getBalance();
        final Balance expectedBalance = new Balance(0);
        assertEquals(expectedBalance, actualBalance);
    }

    @Test
    public void should_withdrawal_300euros_with_success() throws OperationNotAllowedException {
        account.deposit(500);
        account.withdraw(300);
        final Balance expectedBalance = new Balance(200);
        assertEquals(expectedBalance, account.getBalance());
    }

    @Test
    public void should_withdrawal_100euros_and_200euros_with_success() throws OperationNotAllowedException {
        account.deposit(500);
        account.withdraw(100);
        account.withdraw(200);
        final Balance expectedBalance = new Balance(200);
        assertEquals(expectedBalance, account.getBalance());
    }

    @Test
    public void should_not_withdraw_negative_amount() {
        try{
            account.deposit(500);
            account.withdraw(600);
        }catch(Exception e){
            this.exception = e.getMessage();
        }
        assertTrue(this.exception.equals("Your balance is insufficient : " +
                "you want to withdraw 600.000000 then your account has a balance 500.000000"));
    }

    @Test
    public void should_not_withdraw_greater_than_balance() {
        Exception thrown = assertThrows(
                IllegalArgumentException.class,
                () -> account.withdraw(-200));
        assertTrue(thrown.getMessage().equals("The amount must be positive"));
    }

    @Test
    public void should_writeTo_when_two_entries_are_expected() throws OperationNotAllowedException {
        account.deposit(5000);
        account.withdraw(2000);
        account.printHistory(statementHistoryPrinter);
        Mockito.verify(statementHistoryPrinter, times(2)).print(ArgumentMatchers.any(StatementHistoryEntry.class));
    }

    @Test
    public void should_writeTo_when_one_entry_is_expected() throws OperationNotAllowedException {
        account.deposit(5000);
        account.printHistory(statementHistoryPrinter);
        Mockito.verify(statementHistoryPrinter, times(1)).print(entryArgumentCapture.capture());
        final StatementHistoryEntry actualStatementHistoryEntry = entryArgumentCapture.getValue();
        assertNotNull(actualStatementHistoryEntry);
        assertEquals(EBankOperations.DEPOSIT, actualStatementHistoryEntry.operationType());
        assertNotNull(actualStatementHistoryEntry.operationDate());
        assertEquals(new Amount(5000), actualStatementHistoryEntry.amount());
        assertEquals(new Balance(5000), actualStatementHistoryEntry.balance());
    }

    @Test
    public void should_writeTo_when_no_entry_found() {
        account.printHistory(statementHistoryPrinter);
        Mockito.verifyNoMoreInteractions(statementHistoryPrinter);
    }


}
