package com.sgcib.bankaccount.infrastructure;

import com.sgcib.bankaccount.domain.Amount;
import com.sgcib.bankaccount.domain.Balance;
import com.sgcib.bankaccount.domain.StatementHistoryEntry;
import com.sgcib.bankaccount.domain.common.EBankOperations;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StatementHistoryPrinterTest {

    private PrintStream sysOut;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private static final LocalDateTime now = LocalDateTime.now();

    @BeforeEach
    public void setUpStreams() {
        sysOut = System.out;
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void revertStreams() {
        System.setOut(sysOut);
    }

    @Test
    public void should_print_balance_with_operation_deposit() {
        final StatementHistoryEntry statementHistoryEntry = new StatementHistoryEntry(EBankOperations.DEPOSIT, now,
                new Amount(1000), new Balance(2000)); // autoboxing

        new StatementHistoryPrinterImpl().print(statementHistoryEntry);

        assertEquals(outContent.toString().trim(),
                "Operation='DEPOSIT' | Date='"+now+"' | Amount='1000.0' | Balance='2000.0'");
    }

    @Test
    public void should_print_balance_with_operation_wittdrawal() {
        final StatementHistoryEntry statementHistoryEntry = new StatementHistoryEntry(EBankOperations.WITHDRAWAL,
                now,
                new Amount(400),
                new Balance(5000));
        new StatementHistoryPrinterImpl().print(statementHistoryEntry);
        assertEquals(outContent.toString().trim(),
                "Operation='WITHDRAWAL' | Date='"+now+"' | Amount='400.0' | Balance='5000.0'");
    }

}
