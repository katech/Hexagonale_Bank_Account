Feature: In order to save money
  As a bank client
  I want to make a deposit in my account

  Scenario Outline: i want to make deposit in my account;
    Given i have a bank account with balance <initial_balance>
    When i am depositing <deposit> in my account
    Then my balance must be <final_balance>

    Examples:
      | initial_balance | deposit | final_balance |
      | 0               | 200     | 200           |
      | 30              | 300     | 330           |
      | 30              | 0       | 30            |
