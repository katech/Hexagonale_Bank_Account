Feature: In order to check my operations
  As a bank client
  I want to see the history (operation, date, amount, balance) of my operations

  Background:
    Given we are the "15/06/2022"
    Given i have a bank account with balance 8000
    And i am depositing 600 in my account
    And i am withdrawing 4000 from my account


  Scenario: checking the history;
    When i print my statement i got this output
      | operation  | date       | amount | balance |
      | DEPOSIT    | 15/06/2022 | 600    | 8600    |
      | WITHDRAWAL | 15/06/2022 | 4000   | 4600    |


  Scenario: executing some valid operations and checking the history;
    Given i am withdrawing 500 from my account
    And i am depositing 2000 in my account
    When i print my statement i got this output
      | operation  | date       | amount | balance |
      | DEPOSIT    | 15/06/2022 | 600    | 8600    |
      | WITHDRAWAL | 15/06/2022 | 4000   | 4600    |
      | WITHDRAWAL | 15/06/2022 | 500    | 4100    |
      | DEPOSIT    | 15/06/2022 | 2000   | 6100    |

  Scenario: executing some invalid operations and checking the history;
    When i am withdrawing 7000 from my account
    Then i get the following error : "Your balance is insufficient : you want to withdraw 7000.000000 then your account has a balance 4600.000000"
    When i print my statement i got this output
      | operation  | date       | amount | balance |
      | DEPOSIT    | 15/06/2022 | 600    | 8600    |
      | WITHDRAWAL | 15/06/2022 | 4000   | 4600    |
