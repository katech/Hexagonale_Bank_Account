Feature: In order to retrieve some or all of my savings
  As a bank client
  I want to make a withdrawal from my account

  Scenario Outline: i want to retrieve some of my saving;
    Given i have a bank account with balance <initial_balance>
    When i am withdrawing <withdrawal> from my account
    Then my balance must be <final_balance>

    Examples:
      | initial_balance | withdrawal | final_balance |
      | 500             | 250        | 250           |
      | 30              | 0          | 30            |
